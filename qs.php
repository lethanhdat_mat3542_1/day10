<?php

function qs($key, $qs, $ans = [])
{
    echo ' <div class="col-4">
            <div class="my-2 font-weight-bold">
               Question ' . strval($key + 1) . ' : ' . $qs['qs'] . '
            </div> ';
?>

<?php
    foreach ($qs['answer'] as $key_answer => $answer) {
        $selected = !empty($ans) && $ans[$key] == $key_answer  ? "checked=checked" : "";
        if (!empty($ans) && $qs['solution'] == $key_answer) {
            echo '<label class="container-checkbox">
                    <span style="color: green">' . $key_answer . ' : ' . $answer . '  </span>
                    <input disabled ' . $selected . ' value="' . $key_answer . '" type="radio" name="checkbox' . $key . '"" />
                    <span style="background-color: green" class="checkmark"></span>
                </label>';
        } else {
            if (!empty($ans) && $ans[$key] == $key_answer) {
                echo '<label class="container-checkbox">
                    <span style="color: red">' . $key_answer . ' : ' . $answer . '  </span>
                    <input disabled ' . $selected . ' value="' . $key_answer . '" type="radio" name="checkbox' . $key . '"" />
                    <span style="background-color: red" class="checkmark"></span>
                </label>';
            } else {
                echo '<label class="container-checkbox">
                    <span style="color: ">' . $key_answer . ' : ' . $answer . '  </span>
                    <input disabled ' . $selected . ' value="' . $key_answer . '" type="radio" name="checkbox' . $key . '"" />
                    <span style="background-color: " class="checkmark"></span>
                </label>';
            }
        }
    }
    echo '</div>';
}
?>
    